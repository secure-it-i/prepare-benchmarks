This repository contains scripts and artifacts required to automatically build and test the benchmarks in DroidBench, IccBench, and UBCBench. All the benchmarks in DroidBench and UBCBench will be built with min API Level 23 and target API Level 27. Each benchmark will be executed on an emulator with target android-23 and android-27.

## Prerequisites

1. [Android Studio](https://developer.android.com/studio)
2. GNU Bash

## Configuration

1. Set ANDROID_HOME=/path/to/Android/Sdk
2. Use Android Studio's SDK Manager to install platforms 16,17,18,19,22,23,27
3. Use Android Studio's AVD manager to create emulators or AVDs for platforms 16,17,18,19,22,23,27. The name of each AVD should be of the form Nexus_5X_API_i where i=16,17,18,19,22,23 or 27
4. Download [DroidBench (extended)](https://foellix.github.io/ReproDroid/) and extract it in `DroidBench/ReproDroid`
5. Download [ICC-Bench 2.0](https://foellix.github.io/ReproDroid/) and extract it in `Icc-Ubc-Bench/IccBench`
6. Get the [UBCBench](https://github.com/LinaQiu/UBCBench) benchmarks and save them in `Icc-Ubc-Bench/UBCBench`

## Building the benchmarks
1. DroidBench
    * `$ cd DroidBench/ModelReproDroidProject`
    * `$ mkdir ReproDroidProject`
    * Set the variables in `scripts/buildApk.sh` to local paths
    * `$ ./scripts/buildApk.sh`

2. IccBench
    * `$ cd Icc-Ubc-Bench/ModelICCBenchProject`
    * Set the variables in `scripts/buildIccBench.sh` to local paths
    * `$ ./scripts/buildIccBench.sh`

3. UBCBench
    * `$ cd Icc-Ubc-Bench/ModelICCBenchProject`
    * Set the variables in `scripts/buildUbcBench.sh` to local paths
    * `$ ./scripts/buildUbcBench.sh`

## Executing the benchmarks
1. Set the variables in `scripts/test-benchmarks.sh`
2. `$ ./scripts/test-benchmarks.sh`
3. Test the benchmarks in the emulator and follow the instructions

## Verifying failed benchmarks (optional)
3 benchmarks in UBCBench and 32 benchmarks in DroidBench fail to execute on emulators running android-23 and android-27. However, the original benchmarks were designed to target lower API Levels. Unfortunately, these 35 benchmarks fail to execute on API Levels for which they were designed as well. You can verify this as follows:

1. Collect the benchmark names that failed to execute in the step titled *Executing the benchmarks*
2. Collect the original apks for these benchmarks from `DroidBench/ReproDroid` and `Icc-Ubc-Bench/UBCBench`
3. Set the variables in `scripts/test-failed-apks.sh`
4. `$ ./scripts/test-failed-apks.sh`  

## Attribution

Copyright (c) 2019/2018, Kansas State University

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

Contributors:

1. Joydeep Mitra [Creator + Developer]
2. Venkatesh-Prasad Ranganath [Advisor]
