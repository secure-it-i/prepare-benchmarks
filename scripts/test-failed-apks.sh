#!/bin/bash
# This script installs an apk and enables its testing in an emulator with target API Level as specified in the apk's manifest.
# Set the variables before running the script
# Create AVDS named Nexus_5X_API_i where i is a target API Level

ANDROID_HOME=~/Android/Sdk
APKS=/path/to/failed-apks
LOG=/path/to/logfile

echo ""
echo "You will use the following settings:"
echo "  ANDROID_HOME=$ANDROID_HOME"
echo "  APKS=$APKS"
echo "  LOG=$LOG"
read -p "Are these settings correct? (yn) " ans

if [[ $ans != "y" ]] ; then
    echo "Change your setting in scripts/test-benchmarks.sh and rerun it."
    exit
fi
echo ""

if [ -e $LOG ] ; then
  rm $LOG
fi

wait_for_boot_completion () {
    $ANDROID_HOME/platform-tools/adb wait-for-device
    while [ "`$ANDROID_HOME/platform-tools/adb shell getprop sys.boot_completed | tr -d '\r' `" != "1" ] ; do
        sleep 1
    done
}

for b in `ls -1 $APKS/*.apk` ; do
  AppId=`$ANDROID_HOME/tools/bin/apkanalyzer manifest application-id $b`
  i=`$ANDROID_HOME/tools/bin/apkanalyzer manifest target-sdk $b`
  echo "Executing $b against API $i"
  os=`uname -s`
  if [[ $os = "Linux" ]]; then
      $ANDROID_HOME/emulator/emulator -avd Nexus_5X_API_$i -wipe-data \
          -no-boot-anim -no-snapshot -selinux permissive \
          -use-system-libs -gpu off &
  else
      $ANDROID_HOME/emulator/emulator -avd Nexus_5X_API_$i -wipe-data \
          -no-boot-anim -no-snapshot -selinux permissive &
  fi
  wait_for_boot_completion
  emulator_pid=$!
  $ANDROID_HOME/platform-tools/adb install -t $b
  read -p "Press n to indicate test failed " answer
  if [[ $answer == "n" ]] ; then
      echo "$b failed on $i" >> $LOG
  fi
  $ANDROID_HOME/platform-tools/adb shell pm uninstall $AppId
  $ANDROID_HOME/platform-tools/adb emu kill
  wait $emulator_pid

  ps -p $emulator_pid
  if [ $? -ne 1 ] ; then
      kill -9 $emulator_pid
  fi

  $ANDROID_HOME/platform-tools/adb kill-server
done
