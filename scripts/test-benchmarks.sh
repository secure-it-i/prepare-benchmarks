#!/bin/bash
# This script installs an apk and enables its testing in an emulator with target API Level 22 and 27.
# Set the variables before running the script
# Create AVDS named Nexus_5X_API_22 and Nexus_5X_API_22

ANDROID_HOME=~/Android/Sdk
APKS=/path/to/apks
LOG=/path/to/logfile
MIN_SDK=23
TARGET_SDK=27

echo ""
echo "You will use the following settings:"
echo "  ANDROID_HOME=$ANDROID_HOME"
echo "  APKS=$APKS"
echo "  LOG=$LOG"
read -p "Are these settings correct? (yn) " ans

if [[ $ans != "y" ]] ; then
    echo "Change your setting in scripts/test-benchmarks.sh and rerun it."
    exit
fi
echo ""

if [ -e $LOG ] ; then
  rm $LOG
fi

wait_for_boot_completion () {
    $ANDROID_HOME/platform-tools/adb wait-for-device
    while [ "`$ANDROID_HOME/platform-tools/adb shell getprop sys.boot_completed | tr -d '\r' `" != "1" ] ; do
        sleep 1
    done
}

for i in $MIN_SDK $TARGET_SDK ; do
  echo "Executing $b against API $i"
  os=`uname -s`
  if [[ $os = "Linux" ]]; then
      $ANDROID_HOME/emulator/emulator -avd Nexus_5X_API_$i -wipe-data \
          -no-boot-anim -no-snapshot -selinux permissive \
          -use-system-libs -gpu off &
  else
    $ANDROID_HOME/emulator/emulator -avd Nexus_5X_API_$i -wipe-data \
        -no-boot-anim -no-snapshot -selinux permissive &
  fi
  wait_for_boot_completion
  emulator_pid=$!
  for b in `ls -1 $APKS/*.apk` ; do
    AppId=`$ANDROID_HOME/tools/bin/apkanalyzer manifest application-id $b`
    if [ $i == 22 ] ; then
      $ANDROID_HOME/platform-tools/adb install -t $b
    else
      $ANDROID_HOME/platform-tools/adb install -g $b
    fi
    read -p "Press n to indicate test failed " answer
    if [[ $answer == "n" ]] ; then
        echo "$b failed on $i" >> $LOG
    fi
    $ANDROID_HOME/platform-tools/adb shell pm uninstall $AppId
  done
  $ANDROID_HOME/platform-tools/adb emu kill
  wait $emulator_pid

  ps -p $emulator_pid
  if [ $? -ne 1 ] ; then
      kill -9 $emulator_pid
  fi

  $ANDROID_HOME/platform-tools/adb kill-server
done
