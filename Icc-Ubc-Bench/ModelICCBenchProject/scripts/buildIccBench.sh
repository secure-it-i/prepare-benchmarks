#!/bin/bash
#Change variables before running script
#Run script from ModelReproDroidProject folder
#Set ANDROID_HOME before running this script

ICCBENCH_HOME=/path/to/ICCBENCH
BENCHMARK_SUITE=$ICCBENCH_HOME/ICCBench20/benchmark/projects
OUTPUT=output-apks
LOG=logs

if [ -z ${ANDROID_HOME+x} ] ; then
  echo "ANDROID_HOME is unset. Please set it and rerun the script"
  exit
else
  echo "ANDROID_HOME is set to '$ANDROID_HOME'"
fi

echo ""
echo "You will use the following settings:"
echo "  ICCBENCH_HOME=$ICCBENCH_HOME"
echo "  BENCHMARK_SUITE=$BENCHMARK_SUITE"
echo "  OUTPUT=$OUTPUT"
echo "  LOG=$LOG"
read -p "Are these settings correct? (yn) " ans

if [[ $ans != "y" ]] ; then
    echo "Change your setting in scripts/buildIccBench.sh and rerun it."
    exit
fi
echo ""

if [ -e $LOG ];
then
  rm -rf $LOG
  echo "cleaned up old logs"
  mkdir $LOG
else
  mkdir $LOG
fi

echo "created fresh $LOG"

build_iccbench() {
  if [ -e $OUTPUT ];
  then
    rm -rf $OUTPUT
    echo "cleaned up old $OUTPUT"
    mkdir $OUTPUT
  else
    mkdir $OUTPUT
  fi

  echo "created fresh $OUTPUT"

  for i in `ls -1d $BENCHMARK_SUITE/*` ; do
    echo "Processing folder $i ... "
    for j in `ls -1d $i/*`; do
      n=`echo $j | rev | cut -d'/' -f1 | rev`
      echo "Start build process for $n ... "
      rm -rf app/build/
      rm -rf app/src
      rm app/build.gradle
      echo "cleaned app/ folder ... "
      cp $j/build.gradle app/
      echo "apply from: 'minify.gradle'" >> app/build.gradle
      cp -r $j/src app/
      echo "prepared app/ folder ... "
      BUILD_STATUS=`./gradlew assembleDebug | grep "BUILD SUCCESSFUL" | wc -l | bc`
      if [ $BUILD_STATUS -ge 1 ];
      then
        echo "build over ... "
        cp app/build/outputs/apk/debug/app-debug.apk $OUTPUT/$n.apk
        echo "successfully created $n.apk" >> $LOG/buildIccApk.log
      else
        echo "build failed for $j" >> $LOG/buildIccApk.log
      fi
    done
  done

}


build_iccbench
