#!/bin/bash
#Change variables before running script

BENCHMARK_UBCBENCH_SUITE=~/UBCBench/android-studio-project
OUTPUT_UBCBENCH=output-ubcbench-apks
LOG=logs

if [ -z ${ANDROID_HOME+x} ] ; then
  echo "ANDROID_HOME is unset. Please set it and rerun the script"
  exit
else
  echo "ANDROID_HOME is set to '$ANDROID_HOME'"
fi

echo ""
echo "You will use the following settings:"
echo "  BENCHMARK_UBCBENCH_SUITE=$BENCHMARK_UBCBENCH_SUITE"
echo "  OUTPUT=$OUTPUT_UBCBENCH"
echo "  LOG=$LOG"
read -p "Are these settings correct? (yn) " ans

if [[ $ans != "y" ]] ; then
    echo "Change your setting in scripts/buildUbcBench.sh and rerun it."
    exit
fi
echo ""

if [ -e $LOG ];
then
  rm -rf $LOG
  echo "cleaned up old logs"
  mkdir $LOG
else
  mkdir $LOG
fi

echo "created fresh $LOG"


build_ubcbench () {
  if [ -e $OUTPUT_UBCBENCH ];
  then
    rm -rf $OUTPUT_UBCBENCH
    echo "cleaned up old $OUTPUT_UBCBENCH"
    mkdir $OUTPUT_UBCBENCH
  else
    mkdir $OUTPUT_UBCBENCH
  fi

  echo "created fresh $OUTPUT_UBCBENCH"

  for j in `ls -1d $BENCHMARK_UBCBENCH_SUITE/*`; do
    n=`echo $j | rev | cut -d'/' -f1 | rev`
    echo "Start build process for $n ... "
    rm -rf app/build/
    rm -rf app/src
    rm app/build.gradle
    echo "cleaned app/ folder ... "
    cp $j/app/build.gradle app/
    echo "apply from: 'minify-ubcbench.gradle'" >> app/build.gradle
    cp -r $j/app/src app/
    echo "prepared app/ folder ... "
    BUILD_STATUS=`./gradlew assembleDebug | grep "BUILD SUCCESSFUL" | wc -l | bc`
    if [ $BUILD_STATUS -ge 1 ];
    then
      echo "build over ... "
      cp app/build/outputs/apk/debug/app-debug.apk $OUTPUT_UBCBENCH/$n.apk
      echo "successfully created $n.apk" >> $LOG/buildUbcApk.log
    else
      echo "build failed for $j" >> $LOG/buildUbcApk.log
    fi
  done
}

build_ubcbench
