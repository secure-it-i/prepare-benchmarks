#!/bin/bash
#Change variables before running script
#Run script from ModelReproDroidProject folder

MODEL_REPRODROID_HOME=/home/joydeep/BenchPress/prepare-benchmarks/DroidBench/ModelReproDroidProject
REPRODROID_PROJ=$MODEL_REPRODROID_HOME/ReproDroidProject
REPRODROID_HOME=~/ReproDroid
BENCHMARK_SUITE=$REPRODROID_HOME/DroidBenchExtended/benchmark/projects/
OUTPUT=$MODEL_REPRODROID_HOME/output-apks
LOG=$MODEL_REPRODROID_HOME/logs

if [ -z ${ANDROID_HOME+x} ] ; then
  echo "ANDROID_HOME is unset. Please set it and rerun the script"
  exit
else
  echo "ANDROID_HOME is set to '$ANDROID_HOME'"
fi

# echo ""
# echo "You will use the following settings:"
# echo "  MODEL_REPRODROID_HOME=$MODEL_REPRODROID_HOME"
# echo "  REPRODROID_PROJ=$REPRODROID_PROJ"
# echo "  REPRODROID_HOME=$REPRODROID_HOME"
# echo "  BENCHMARK_SUITE=$BENCHMARK_SUITE"
# echo "  OUTPUT=$OUTPUT"
# echo "  LOG=$LOG"
# read -p "Are these settings correct? (yn) " ans
#
# if [[ $ans != "y" ]] ; then
#     echo "Change your setting in scripts/buildApk.sh and rerun it."
#     exit
# fi
# echo ""

if [ -e $LOG ];
then
  rm -rf $LOG
  echo "cleaned up old logs"
  mkdir $LOG
else
  mkdir $LOG
fi

echo "created fresh $LOG"

if [ -e $OUTPUT ];
then
  rm -rf $OUTPUT
  echo "cleaned up old $OUTPUT"
  mkdir $OUTPUT
else
  mkdir $OUTPUT
fi

echo "created fresh $OUTPUT"

for i in `find $BENCHMARK_SUITE -name "*.zip"` ; do
  n=`basename $i | cut -d'.' -f1`
  m=`echo $i | rev | cut -d'/' -f2 | rev`
  rm -rf $REPRODROID_PROJ/*
  echo "cleaned $REPRODROID_PROJ"
  unzip $i -d $REPRODROID_PROJ/
  echo "Unzipped `basename $i` successfully to $REPRODROID_PROJ"
  if [ -e $REPRODROID_PROJ/*/app/build.gradle ] ; then
    # If gradle file exists then configure gradle with appropriate API Levels.
    echo "gradle file exists for $n-$m.apk" >> $LOG/buildApk.log
    cd $REPRODROID_PROJ/*
    sed 's/compileSdkVersion [0-9][0-9]/compileSdkVersion 27/g' app/build.gradle > app/build1.gradle
    mv app/build1.gradle app/build.gradle
    sed 's/buildToolsVersion "*.*.*"/buildToolsVersion "28.0.3"/g' app/build.gradle > app/build1.gradle
    mv app/build1.gradle app/build.gradle
    sed 's/minSdkVersion [0-9][0-9]/minSdkVersion 23/g' app/build.gradle > app/build1.gradle
    mv app/build1.gradle app/build.gradle
    sed 's/targetSdkVersion [0-9][0-9]/targetSdkVersion 27/g' app/build.gradle > app/build1.gradle
    mv app/build1.gradle app/build.gradle
    sed 's/minifyEnabled false/minifyEnabled true/g' app/build.gradle > app/build1.gradle
    mv app/build1.gradle app/build.gradle
    sed 's/release/debug/g' app/build.gradle > app/build1.gradle
    mv app/build1.gradle app/build.gradle
    sed 's/proguard-rules.pro/..\/..\/..\/proguard-rules.pro/g' app/build.gradle > app/build1.gradle
    mv app/build1.gradle app/build.gradle
    sed 's/com.android.support:appcompat-v7:[0-9][0-9].[0-9].[0-9]/com.android.support:appcompat-v7:27.1.1/g' app/build.gradle > app/build1.gradle
    mv app/build1.gradle app/build.gradle
    echo "apply from: '../../support.gradle'" >> build.gradle
    chmod +x gradlew
  else
    # If gradle file is absent then use the template gradle file.
    cp $REPRODROID_PROJ/*/AndroidManifest.xml app/src/main
    rm -rf app/src/main/res
    echo "cleaned old res files"
    cp -r $REPRODROID_PROJ/*/res app/src/main
    echo "res done"
    rm -rf app/src/main/java
    echo "cleaned old java files"
    cp -r $REPRODROID_PROJ/*/src app/src/main
    mv app/src/main/src app/src/main/java
    echo "java done"
    rm -rf app/build
    echo "cleaning old build"
  fi
  echo "starting build for `basename $i` ... "
  pwd >> $LOG/buildApk.log
  BUILD_STATUS=`./gradlew assembleDebug | grep "BUILD SUCCESSFUL" | wc -l | bc`
  if [ $BUILD_STATUS -ge 1 ];
  then
    echo "build over ... "
    if [ -e app/build/outputs/apk/debug/app-debug.apk ] ; then
      cp app/build/outputs/apk/debug/app-debug.apk $OUTPUT/$n-$m.apk
      echo "successfully created $n-$m.apk" >> $LOG/buildApk.log
    else
      if [ -e app/build/outputs/apk/app-debug.apk ] ; then
        cp app/build/outputs/apk/app-debug.apk $OUTPUT/$n-$m.apk
        echo "successfully created $n-$m.apk" >> $LOG/buildApk.log
      else
        echo "$n-$m.apk apk not found" >> $LOG/buildApk.log
      fi
    fi
  else
    echo "build failed for $i" >> $LOG/buildApk.log
  fi
  cd $MODEL_REPRODROID_HOME
done
